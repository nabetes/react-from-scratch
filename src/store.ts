import GlobalState from 'react-global-state-hooks';
import * as IGlobalState from 'react-global-state-hooks/lib/GlobalStoreTypes';

interface ICountActions extends IGlobalState.ActionCollectionResult<IGlobalState.IActionCollection<number>> {
  decrease: (decrease: number) => (setter: IGlobalState.StateSetter<number>, state: number) => Promise<void>,
  increase: (increase: number) => (setter: IGlobalState.StateSetter<number>, state: number) => Promise<void>,
}

class CountActions implements IGlobalState.IActionCollection<number> {

  [key: string]: IGlobalState.IAction<number>;

  decrease = (decrease: number) => async (setter: IGlobalState.StateSetter<number>, state: number) => {
    setter(state - decrease);
  }

  increase = (increase: number) => async (setter: IGlobalState.StateSetter<number>, state: number) => {
    setter(state + increase);
  }

}

const store = new GlobalState(0, new CountActions(), 'COUNT');

export const useCount = store.getHook<ICountActions>();
export const useCountDecoupled = store.getHookDecoupled<ICountActions>();

export default useCount;
