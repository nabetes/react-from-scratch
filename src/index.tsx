import React, { useCallback } from 'react';
import ReactDOM from 'react-dom';
import { useCount } from 'store';

function Hello() {
  const [state, actions] = useCount();
  const onPress = useCallback(() => actions.increase(1), []);

  return (<>
    <button style={{ display: 'block', margin: '0 auto' }} onClick={onPress} >
      1 Count: {state}
    </button>
  </>);
}

function Hello2() {
  const [state, actions] = useCount();
  const onPress = useCallback(() => actions.decrease(1), []);

  return (<>
    <button style={{ display: 'block', margin: '0 auto' }} onClick={onPress} >
      2 Count: {state}
    </button>
  </>);
}

function Orquestador() {
  return (<>
    <Hello />
    <Hello2 />
  </>);
}

ReactDOM.render(
  <>
    <Orquestador />
  </>,
  document.getElementById('root'),
);
